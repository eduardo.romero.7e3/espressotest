package com.example.espressotest;

import androidx.test.espresso.ViewAssertion;
import androidx.test.espresso.intent.Intents;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    static String USER_TO_BE_TYPED = "NetWayFan";
    static String PASS_TO_BE_TYPED = "Password67";

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule
            = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void check_elements_displayed_correctly() {
        onView(withId(R.id.textView)).check(matches(isDisplayed()));
        onView(withId(R.id.button)).check(matches(isDisplayed()));
    }

    @Test
    public void check_text_on_elements() {
        onView(withId(R.id.textView)).check(matches(withText("Hello friends, it is I, NetWay fan")));
        onView(withId(R.id.button)).check(matches(withText("NEXT")));
    }

    @Test
    public void check_button_is_clickable_and_does_its_damn_job() {
        onView(withId(R.id.button)).check(matches(isClickable()));
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.button)).check(matches(withText("BACK")));
    }

    @Test
    public void login_form_behavior() {
        onView(withId(R.id.nameEditText)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.passwordEditText)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();

        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.button)).check(matches(withText("LOGGED")));
        //Initially this fails because the text in our button changes to "BACK" when it is clicked.
        //By changing this we can make this test function work.
    }

    @Test
    public void activity_change_behavior(){
        Intents.init();
        onView(withId(R.id.nameEditText)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.passwordEditText)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.button)).perform(click());
        intended(hasComponent(SecondActivity.class.getName()));
    }

    @Test
    public void activity_change_and_back_behavior(){
        Intents.init();
        onView(withId(R.id.nameEditText)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.passwordEditText)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.button)).perform(click());
        intended(hasComponent(SecondActivity.class.getName()));
        onView(withId(R.id.backButton)).perform(click());
        intended(hasComponent(MainActivity.class.getName()));
        Intents.release();
    }

    @Test
    public void activity_change_with_back_button_behavior(){
        Intents.init();
        onView(withId(R.id.nameEditText)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.passwordEditText)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.button)).perform(click());
        intended(hasComponent(SecondActivity.class.getName()));
        pressBack();
        intended(hasComponent(MainActivity.class.getName()));
        Intents.release();
    }

    @Test
    public void big_test(){
        Intents.init();

        onView(withId(R.id.nameEditText)).perform(typeText(USER_TO_BE_TYPED));
        closeSoftKeyboard();
        onView(withId(R.id.passwordEditText)).perform(typeText(PASS_TO_BE_TYPED));
        closeSoftKeyboard();

        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.button)).perform(click());

        intended(hasComponent(SecondActivity.class.getName()));
        onView(withId(R.id.backTextView)).check(matches(withText("Welcome back, " + USER_TO_BE_TYPED + "!")));

        pressBack();
        intended(hasComponent(MainActivity.class.getName()));

        onView(withId(R.id.nameEditText)).check(matches(withText("")));
        onView(withId(R.id.passwordEditText)).check(matches(withText("")));

        Intents.release();
    }
}
