package com.example.espressotest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView, name, pass;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        button = findViewById(R.id.button);
        name = findViewById(R.id.nameEditText);
        pass = findViewById(R.id.passwordEditText);

        button.setOnClickListener(v -> {
            if (!name.getText().toString().isEmpty() && !pass.getText().toString().isEmpty()) {
                if(!button.getText().toString().equals("LOGGED")) button.setText("LOGGED");
                else{
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("username", name.getText().toString());
                    startActivity(intent);
                }
            } else button.setText("BACK");
        });
    }
}